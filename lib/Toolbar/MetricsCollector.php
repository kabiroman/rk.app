<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Toolbar;

use Bitrix\Main\Diag\SqlTracker;

/**
 * Class MetricsCollector
 *
 * @package Rk\App\Toolbar
 */
class MetricsCollector
{

    /**
     * @var MetricsCollector
     */
    private static $instance;

    /**
     * @var float
     */
    private $startTime;

    /**
     * @var float
     */
    private $startPageTime;

    /**
     * @var int
     */
    private $startPageMemory;

    /**
     * @var int
     */
    private $queryCount = 0;

    /**
     * @var float
     */
    private $queryTime = 0.0;

    /**
     * Initializer
     */
    public static function init()
    {
        /** @global \CDatabase $DB */
        global $DB;
        $DB->ShowSqlStat = true;

        self::$instance = new self($_SERVER['REQUEST_TIME_FLOAT'], microtime(true), memory_get_peak_usage());
    }

    public static function onProlog()
    {
        global $DB;
        if ($DB->sqlTracker instanceof SqlTracker) {
            $self = static::getInstance();
            $self->queryCount = $DB->sqlTracker->getCounter();
            $self->queryTime = $DB->sqlTracker->getTime();
        }
    }

    /**
     * @return MetricsCollector
     */
    public static function getInstance(): MetricsCollector
    {
        if ( ! self::$instance) {
            throw new \RuntimeException('Toolbar agent not initialized!');
        }
        return self::$instance;
    }

    /**
     * MetricsCollector constructor.
     *
     * @param float $startTime
     * @param float $startPageTime
     * @param int   $startPageMemory
     */
    protected function __construct(float $startTime, float $startPageTime, int $startPageMemory)
    {
        $this->startTime = $startTime;
        $this->startPageTime = $startPageTime;
        $this->startPageMemory = $startPageMemory;
    }

    /**
     * @return float
     */
    public function getTimeUsage(): float
    {
        $actualTime = microtime(true);

        return $actualTime - $this->startTime;
    }

    /**
     * @return float
     */
    public function getPageTimeUsage(): float
    {
        $actualTime = microtime(true);

        return $actualTime - $this->startPageTime;
    }

    /**
     * @return int
     */
    public function getPageMemoryUsage(): int
    {
        $actualMemory = memory_get_peak_usage();

        return $actualMemory - $this->startPageMemory;
    }

    /**
     * @return int
     */
    public function getMemoryUsage(): int
    {
        return memory_get_usage();
    }

    /**
     * @return int
     */
    public function getMemoryLimit(): int
    {
        $memory_limit = ini_get('memory_limit');
        if (preg_match('/^(\d+)(.)$/', $memory_limit, $matches)) {
            if ($matches[2] == 'T') {
                $memory_limit = $matches[1] * 1024 * 1024 * 1024 * 1024;
            } else if ($matches[2] == 'G') {
                $memory_limit = $matches[1] * 1024 * 1024 * 1024;
            } else if ($matches[2] == 'M') {
                $memory_limit = $matches[1] * 1024 * 1024;
            } else if ($matches[2] == 'K') {
                $memory_limit = $matches[1] * 1024;
            }
        }

        return $memory_limit;
    }

    /**
     * @return int
     */
    public function getQueryCount(): int
    {
        return $this->queryCount;
    }

    /**
     * @return float
     */
    public function getQueryTime(): float
    {
        return $this->queryTime;
    }
}