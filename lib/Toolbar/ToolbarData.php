<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Toolbar;

use Bitrix\Main\Application;
use Rk\App\Dumper\DumpStorage\DumpStorage;

/**
 * Class ToolbarData
 *
 * @package Rk\App\Toolbar
 */
class ToolbarData
{
    public $__isInitialized = false;
    public $httpCode;
    public $url;
    public $totalTime;
    public $pageTime;
    public $totalMemory;
    public $pageMemory;
    public $login;
    public $site;
    public $logs = [];
    public $logItems = [];
    public $timeItems = [];
    public $criticalTime = false;
    public $memoryItems = [];
    public $criticalMemory = false;
    public $siteItems = [];
    public $userItems = [];
    public $debug = false;
    public $dumps;
    public $queryCount;
    public $queryTime;
    public $queryItems = [];

    /**
     * @return ToolbarData
     */
    public static function getInstance(): ToolbarData
    {
        global $USER;

        return new self(
            Application::getInstance(),
            $USER
        );
    }

    /**
     * ToolbarData constructor.
     *
     * @param Application $application
     * @param \CUser      $user
     */
    public function __construct(Application $application, \CUser $user)
    {
        try {
            $collector = MetricsCollector::getInstance();
            $dumpStorage = DumpStorage::getInstance();
        } catch (\Throwable $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
            return;
        }
        global $DB;
        $DB->ShowSqlStat = false;
        $sqlTracker = Application::getInstance()->getConnection()->getTracker();

        $this->__isInitialized = true;
        $this->httpCode = http_response_code();
        $this->url = $application->getContext()->getRequest()->getRequestedPageDirectory();
        $this->login = $user->GetLogin() ? $user->GetLogin() : 'anon.';
        $this->pageTime = (round($collector->getPageTimeUsage(), 3) * 1000).' ms';
        $this->totalTime = (round($collector->getTimeUsage(), 3) * 1000).' ms';
        $this->pageMemory = round($collector->getPageMemoryUsage() / 1024 / 1024, 1).' MiB';
        $this->totalMemory = round($collector->getMemoryUsage() / 1024 / 1024, 1).' MiB';
        $this->site = SITE_ID;
        $this->logs = ($logs = Log::getInstance()->toArray());
        $this->logItems = $this->prepareLogItems($logs);
        $this->timeItems = $this->prepareTimeItems($collector);
        $this->criticalTime = round($collector->getTimeUsage(), 3) * 1000 > 400;
        $this->memoryItems = $this->prepareMemoryItems($collector);
        $this->criticalMemory = ($collector->getMemoryLimit() - $collector->getMemoryUsage()) / ($collector->getMemoryUsage() / 100)
            < 15;
        $this->siteItems = $this->prepareSiteItems();
        $this->userItems = $this->prepareUserItems($user);
        $this->dumps = count($dumpStorage->getDumps());
        $this->queryCount = $sqlTracker->getCounter() - $collector->getQueryCount();
        $this->queryTime = (round($sqlTracker->getTime() - $collector->getQueryTime(), 4) * 1000).' ms';
        $this->queryItems = $this->prepareQueryItems($this->queryCount, $this->queryTime);
    }

    /**
     * @return array
     */
    public function getDumps(): array
    {
        return DumpStorage::getInstance()->getDumps();
    }

    /**
     * @return bool
     */
    public function isInitialized(): bool
    {
        return $this->__isInitialized;
    }

    /**
     * @return array
     */
    private function prepareSiteItems(): array
    {
        $includes = [
            'REQUEST_METHOD' => 'Request method',
            'DOCUMENT_ROOT'  => 'Document root',
            'SCRIPT_NAME'    => 'Script file',
        ];
        $result = [
            $this->createItemArray('HTTP status', http_response_code()),
        ];
        foreach ($includes as $itemKey => $itemName) {
            $result[] = $this->createItemArray($itemName, (string)$_SERVER[$itemKey]);
        }

        return $result;
    }

    /**
     * @param $queryCount
     * @param $queryTime
     *
     * @return array
     */
    private function prepareQueryItems($queryCount, $queryTime)
    {
        return [
            $this->createItemArray('Database Queries', $queryCount),
            $this->createItemArray('Query time', $queryTime),
        ];
    }

    /**
     * @param \CUser $user
     *
     * @return array
     */
    private function prepareUserItems(\CUser $user): array
    {
        return [
            $this->createItemArray('Authorized', $user->GetID() ? "Yes" : "No"),
            $this->createItemArray('Is admin', $user->IsAdmin() ? 'Yes' : 'No'),
            $this->createItemArray('User ID', (string)$user->GetID()),
        ];
    }

    /**
     * @param MetricsCollector $agent
     *
     * @return array
     */
    private function prepareTimeItems(MetricsCollector $agent): array
    {
        return [
            $this->createItemArray('Total time', (string)$this->totalTime),
            $this->createItemArray('Page time', (string)$this->pageTime),
            $this->createItemArray(
                'Initialization time',
                (round($agent->getTimeUsage() - $agent->getPageTimeUsage(), 3) * 1000).' ms'
            )
        ];
    }

    /**
     * @param MetricsCollector $agent
     *
     * @return array
     */
    private function prepareMemoryItems(MetricsCollector $agent): array
    {
        return [
            $this->createItemArray('Peak memory usage', (string)$this->totalMemory),
            $this->createItemArray('Page memory usage', (string)$this->pageMemory),
            $this->createItemArray('PHP memory limit', round($agent->getMemoryLimit() / 1024 / 1024, 1).' MiB'),
        ];
    }

    /**
     * @param array $logs
     *
     * @return array
     */
    private function prepareLogItems(array $logs): array
    {
        $errTypes = [];
        foreach ($logs as $logItem) {
            switch (true) {
                case (bool)preg_match('/USER_ERROR/', $logItem[0]):
                    $errTypes['User errors'][] = $logItem;
                    break;
                case (bool)preg_match('/USER_NOTICE/', $logItem[0]):
                    $errTypes['User notices'][] = $logItem;
                    break;
                case (bool)preg_match('/USER_WARNING/', $logItem[0]):
                    $errTypes['User warnings'][] = $logItem;
                    break;
                case (bool)preg_match('/USER_DEPRECATED/', $logItem[0]):
                    $errTypes['User deprecations'][] = $logItem;
                    break;
                case (bool)preg_match('/ERROR/', $logItem[0]):
                    $errTypes['Errors'][] = $logItem;
                    break;
                case (bool)preg_match('/NOTICE/', $logItem[0]):
                    $errTypes['Notices'][] = $logItem;
                    break;
                case (bool)preg_match('/WARNING/', $logItem[0]):
                    $errTypes['Warnings'][] = $logItem;
                    break;
                case (bool)preg_match('/DEPRECATED/', $logItem[0]):
                    $errTypes['Deprecations'][] = $logItem;
                    break;
                case (bool)preg_match('/PARSE/', $logItem[0]):
                    $errTypes['Parses'][] = $logItem;
                    break;
                case (bool)preg_match('/STRICT/', $logItem[0]):
                    $errTypes['Strict'][] = $logItem;
                    break;
                default:
                    // no action
            }
        }
        $result = [];
        foreach ($errTypes as $errTypeName => $errTypeValue) {
            $result[] = $this->createItemArray($errTypeName, (string)count($errTypeValue));
        }
        return $result;
    }

    /**
     * @param string $name
     * @param string $value
     *
     * @return array
     */
    private function createItemArray(string $name, string $value)
    {
        return [
            'name'  => $name,
            'value' => $value,
        ];
    }
}