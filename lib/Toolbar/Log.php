<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Toolbar;

use Rk\App\Config\DevConfig;


/**
 * Class Log
 *
 * @package Rk\App\Toolbar
 */
class Log
{
    /**
     * @var Log
     */
    private static $instance;

    /**
     * @var array
     */
    private $storage = [];

    protected $config;

    /**
     * @return Log
     */
    public static function getInstance(): Log
    {
        if ( ! self::$instance) {
            self::$instance = new self(DevConfig::getInstance());
        }

        return self::$instance;
    }

    protected function __construct(DevConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param $errType
     * @param $errStr
     * @param $errFile
     * @param $errLine
     *
     * @return void
     */
    public function add($errType, $errStr, $errFile, $errLine)
    {
        if ( ! $this->config->logBitrixErrors()) {
            $patterns = [
                '/\/bitrix\/components\//',
                '/\/bitrix\/gadgets\//',
                '/\/bitrix\/modules\//',
                '/\/bitrix\/templates\//',
            ];
            foreach ($patterns as $pattern) {
                if (preg_match($pattern, $errFile)) {
                    return;
                }
            }
        }
        $this->storage[] = [$errType, $errStr, $errFile, $errLine];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->storage;
    }
}