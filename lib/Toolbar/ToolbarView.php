<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Toolbar;

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\HttpResponse;
use Rk\App\Config\DevConfig;

/**
 * Class ToolbarView
 *
 * @package Rk\App\Toolbar
 */
class ToolbarView
{
    /**
     * Includes template
     */
    public static function render()
    {
        if (php_sapi_name() !== 'cli') {
            if (Option::get("main", 'update_devsrv') == 'Y') {
                if (Application::getInstance()->getContext()->getResponse() instanceof HttpResponse) {
                    if (DevConfig::getInstance()->showToolbar()) {
                        if ($toolbar = ToolbarData::getInstance()) {
                            if ($toolbar->isInitialized()) {
                                require_once __DIR__.'/../../templates/toolbar.php';
                            }
                        }
                    }
                }
            }
        }
    }
}