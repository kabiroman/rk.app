<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App;


/**
 * Interface ServiceContainerInterface
 *
 * @package Rk\HtmlDumper
 */
interface ServiceContainerInterface
{
    /**
     * @param string $service_name
     *
     * @param string $parent
     *
     * @return mixed
     */
    public function get(string $service_name, string $parent = null);

    /**
     * @return string
     * @example return (string)realpath(__DIR__.'/../config');
     */
    public function getConfigPath(): string;
}