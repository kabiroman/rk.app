<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App;

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Text\StringHelper;
use CAllMain;
use CAllUser;
use CMain;
use CUser;
use Rk\App\Exception\ContainerException;
use Rk\App\Logger\LoggerInterface;
use Rk\Options\App;
use Rk\ORM\OrmService;
use RuntimeException;

/**
 * Class AbstractServiceContainer
 *
 * @package Rk\HtmlDumper
 */
abstract class AbstractServiceContainer implements ServiceContainerInterface
{
    /**
     * @var AbstractServiceContainer
     */
    protected static $instance;

    /**
     * @var string
     */
    private $environment;

    /**
     * @var array
     */
    private $services;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var array
     */
    private $ormParams;

    /**
     * @var string
     */
    private $moduleId;

    /**
     * @var array
     */
    private $requiredParams = [
        'moduleId',
        'log',
    ];

    /**
     * @var string
     */
    private $baseName;

    /**
     * @return AbstractServiceContainer
     * @throws ContainerException
     */
    public static function getInstance(): AbstractServiceContainer
    {
        if ( ! static::$instance) {
            static::$instance = new static();
        }
        $container = static::$instance;

        if ( ! $container->isInitialized()) {
            $configPath = $container->getConfigPath();
            $definesFile = $configPath.'/defines.php';
            $installFile = $configPath.'/install.php';

            if (file_exists($definesFile)) {
                /** @noinspection PhpIncludeInspection */
                require_once $definesFile;
            }
            if (file_exists($installFile)) {
                /** @noinspection PhpIncludeInspection */
                require_once $installFile;
            }
            $orm = [];
            $services = [];
            $parameters = [];

            $ormParamsFile = $configPath.'/orm.php';
            if (file_exists($ormParamsFile)) {
                /** @noinspection PhpIncludeInspection */
                include $ormParamsFile;
            }

            $container->setEnvironment(
                $configPath,
                Option::get("main", 'update_devsrv') == 'Y' ? 'dev' : 'prod'
            );
            $env = $container->getEnvironment();
            $includes = [
                'parameters.php',
                'services.php',
                $env.'/parameters.php',
                $env.'/services.php',
            ];
            foreach ($includes as $name) {
                $file = $configPath.'/'.$name;
                if ( ! file_exists($file)) {
                    throw new ContainerException($container, sprintf('Config file %s is not found!', $file));
                }
                /** @noinspection PhpIncludeInspection */
                require $file;
            }
            $services = $services + ${'services_'.$env};
            $parameters = $parameters + ${'parameters_'.$env};

            foreach ($container->requiredParams as $requiredParam) {
                if ( ! key_exists($requiredParam, $parameters)) {
                    throw new RuntimeException(sprintf('The required parameters "%s" is not specified!', $requiredParam));
                }
            }
            $container->moduleId = $parameters['moduleId'];

            $container->baseName = str_replace(".", "\\", ucwords($parameters['moduleId'], "."));

            $container->setServices($services);
            $container->setParameters($parameters);
            $container->setOrmParams($orm);
        }
        if (isset($container->parameters['modules']) && is_array($container->parameters['modules'])) {
            foreach ($container->parameters['modules'] as $moduleName) {
                if ( ! Loader::includeModule($moduleName)) {
                    throw new RuntimeException(sprintf('The required module "%s" is not installed!', $moduleName));
                }
            }
        }

        return $container;
    }

    /**
     * @return bool
     */
    protected function isInitialized(): bool
    {
        return (is_array($this->services) && ! empty($this->services));
    }

    /**
     * @return string
     */
    public function getEnvironment(): string
    {
        return $this->environment;
    }

    /**
     * @param string $configPath
     * @param string $env
     */
    public function setEnvironment(string $configPath, string $env)
    {
        $definesFile = $configPath.sprintf('/%s/defines.php', $env);
        if (file_exists($definesFile)) {
            /** @noinspection PhpIncludeInspection */
            require_once $definesFile;
        }
        $this->environment = $env;
    }

    /**
     * Container constructor.
     *
     * @param array $services
     */
    protected function setServices(array $services)
    {
        $this->services = $services;
    }

    /**
     * @param array $parameters
     */
    protected function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $ormParams
     */
    protected function setOrmParams(array $ormParams)
    {
        $this->ormParams = $ormParams;
    }

    /**
     * @param string      $service_name
     * @param string|null $parent
     *
     * @return mixed
     * @throws ContainerException
     */
    public function get(string $service_name, string $parent = null)
    {
        if (isset($this->services[$service_name]) && $service = $this->services[$service_name]) {
            if (isset($service['modules']) && is_array($service['modules'])) {
                foreach ($service['modules'] as $moduleId) {
                    if ( ! Loader::includeModule($moduleId)) {
                        throw new ContainerException($this, sprintf('Failed to include "%s" module!', $moduleId));
                    }
                }
            }
        }
        switch ($service_name) {
            case 'container':
                return $this;
            case 'user':
                /** @var CUser|CAllUser $USER */
                global $USER;
                return $USER;
            case 'main':
                /** @var CMain|CAllMain $APPLICATION */
                global $APPLICATION;
                return $APPLICATION;
            case 'application':
                return Application::getInstance();
            case 'logger':
                return $this->detectLogger($parent);
            case 'http_request':
                return Application::getInstance()->getContext()->getRequest();
            case 'options':
                $options = [];
                if (isset($this->parameters['options']) && is_array($this->parameters['options'])) {
                    $options = $this->parameters['options'];
                }
                return new App($this->get('http_request'), $this, $options);
            case 'orm':
                if ( ! isset($this->parameters['orm'])) {
                    throw new RuntimeException('The required parameter "orm" is missing in the configuration parameters!');
                }
                return new OrmService($this->get('logger', 'orm'), $this->parameters['orm']);
            default:
                // no action
        }
        if ( ! isset($this->services[$service_name]) || ! is_array($service = $this->services[$service_name])) {
            throw new ContainerException($this, sprintf('Service "%s" not found!', $service_name));
        }
        if (class_exists($service_name)) {
            $className = $service_name;
        } else {
            if ( ! isset($service['class']) && ! class_exists(isset($service['class']))) {
                throw new ContainerException($this, sprintf('Service class not found "%s"', $service_name));
            }
            $className = $service['class'];
        }
        $arguments = [];
        if (isset($service['arguments']) && is_array($service['arguments'])) {
            foreach ($service['arguments'] as $argument) {
                if (is_string($argument) && (bool)preg_match('/^@/', $argument)) {
                    $sub_name = substr($argument, 1);
                    $arguments[] = $this->get($sub_name, $service_name);
                } else {
                    $arguments[] = $argument;
                }
            }
        }

        return new $className(...$arguments);
    }

    /**
     * @param null|string $type
     *
     * @return LoggerInterface
     */
    protected function detectLogger(?string $type): LoggerInterface
    {
        $namespace = $this->baseName;
        $type = StringHelper::snake2camel($type);

        $className = $namespace."\\Logger\\".(string)$type."Logger";
        if (class_exists($className)) {
            return new $className($this->parameters['log']);
        }
        $className = $namespace."\\Logger\\"."Logger";
        if (class_exists($className)) {
            return new $className($this->parameters['log']);
        }
        $className = __NAMESPACE__."\\Logger\\Logger";

        return new $className($this->parameters['log']);
    }

    /**
     * Blocking the constructor.
     */
    protected function __construct()
    {
    }

    /**
     * Blocking cloning.
     */
    protected function __clone()
    {
    }

    /**
     * @throws \Exception
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize singleton");
    }
}