<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Config;


/**
 * Class Config
 *
 * @package Rk\App\Config
 */
abstract class Config
{

    /**
     * @var bool
     */
    private $initialised = false;

    /**
     * Config constructor.
     *
     * @param string $file
     * @param string $config_var_name
     */
    public function __construct(string $file, $config_var_name = 'config')
    {

        if ( ! file_exists($file)) {
            return;
        }
        /** @noinspection PhpIncludeInspection */
        require_once $file;

        $config = ${$config_var_name};

        if ( ! isset($config) || ! is_array($config)) {
            return;
        }
        foreach ($this as $propName => &$propValue) {
            if (key_exists($propName, $config)) {
                $propValue = $config[$propName];
            }
        }

        $this->initialised = true;
    }

    /**
     * @return bool
     */
    public function isInitialised(): bool
    {
        return $this->initialised;
    }
}