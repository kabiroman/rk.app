<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Config;


/**
 * Class DevConfig
 *
 * @package Rk\App\Config
 */
class DevConfig extends Config
{
    /**
     * @var DevConfig
     */
    private static $instance;

    protected $exception_handler = true;
    protected $error_handler = true;
    protected $log_bitrix_errors = true;
    protected $dumper_function = true;
    protected $show_dump = true;
    protected $show_toolbar = true;

    /**
     * @return DevConfig
     */
    public static function getInstance(): DevConfig
    {
        if ( ! self::$instance) {
            self::$instance = new self(__DIR__.'/../../config.php');
        }

        return self::$instance;
    }

    /**
     * @return bool
     */
    public function exceptionHandler(): bool
    {
        return $this->exception_handler;
    }

    /**
     * @return bool
     */
    public function errorHandler(): bool
    {
        return $this->error_handler;
    }

    /**
     * @return bool
     */
    public function logBitrixErrors(): bool
    {
        return $this->log_bitrix_errors;
    }

    /**
     * @return bool
     */
    public function dumperFunction(): bool
    {
        return (bool)$this->dumper_function;
    }

    /**
     * @return bool
     */
    public function showDump(): bool
    {
        return (bool)$this->show_dump;
    }

    /**
     * @return bool
     */
    public function showToolbar(): bool
    {
        return (bool)$this->show_toolbar;
    }
}