<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Exception;

use Exception;
use Rk\App\ServiceContainerInterface;
use Throwable;

/**
 * Class ContainerException
 *
 * @package Rk\HtmlDumper\Exception
 */
class ContainerException extends Exception
{
    /**
     * @var ServiceContainerInterface
     */
    private $container;

    /**
     * ContainerException constructor.
     *
     * @param ServiceContainerInterface $container
     * @param string                    $message
     * @param int                       $code
     * @param Throwable|null            $previous
     */
    public function __construct(ServiceContainerInterface $container, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->container = $container;

        parent::__construct($message, $code, $previous);
    }
}