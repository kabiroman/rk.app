<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Dumper;


/**
 * Class Dump
 *
 * @package Rk\App\Dumper
 */
class Dump
{
    /**
     * @var mixed
     */
    private $expression;

    /**
     * @var string
     */
    private $file;

    /**
     * @var int
     */
    private $line;

    /**
     * @var array
     */
    private $backtrace;

    /**
     * @var array
     */
    private $result;

    /**
     * Dump constructor.
     *
     * @param mixed $expression
     * @param array $backtrace
     */
    public function __construct($expression, array $backtrace)
    {
        $this->file = $backtrace[0]['file'];
        $this->line = $backtrace[0]['line'];
        $this->expression = $expression;
    }

    /**
     * @return array
     */
    public function getBacktrace(): array
    {
        return $this->backtrace;
    }

    /**
     * @return array
     */
    public function getResult(): array
    {
        return $this->result;
    }

    /**
     * @param array $result
     *
     * @return Dump
     */
    public function setResult(array $result): Dump
    {
        $this->result = $result;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpression()
    {
        return $this->expression;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function getLine(): int
    {
        return $this->line;
    }
}