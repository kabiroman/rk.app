<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Dumper\VarHandler;

use ReflectionClass;
use Rk\App\Dumper\ObjectTracker\ObjectTracker;

/**
 * Class VarHandler
 *
 * @package Rk\App\Dumper\VarHandler
 */
class VarHandler implements VarHandlerInterface
{
    /**
     * @var ObjectTracker
     */
    private $objectTracker;

    /**
     * VarHandler constructor.
     *
     * @param ObjectTracker $tracker
     */
    public function __construct(ObjectTracker $tracker)
    {
        $this->objectTracker = $tracker;
    }

    /**
     * @param mixed $exp
     *
     * @return array
     */
    public function handleExpression($exp): array
    {
        $res = $this->handleExpressionItem($exp);
        $this->objectTracker->clear();

        return $res;
    }

    /**
     * @param mixed       $exp
     * @param string|null $name
     * @param string|null $prefix
     * @param string|null $delimiter
     *
     * @return array
     */
    private function handleExpressionItem($exp, string $name = null, string $prefix = null, string $delimiter = null): array
    {
        $id = null;
        $type = null;
        $len = null;
        $content = null;

        switch (gettype($exp)) {
            case 'object':
                /** @noinspection PhpUndefinedFunctionInspection */
                $id = spl_object_id($exp);
                $type = get_class($exp);
                if ( ! $this->objectTracker->isTracked($exp)) {
                    $this->objectTracker->persist($exp);
                } else {
                    $content = '*RECURSION*';
                    break;
                }
                $len = count((array)$exp);
                $props = (array)$exp;
                foreach ($props as $propertyName => $propertyValue) {
                    switch (true) {
                        case $class = $this->containsClassName($propertyName, $exp):
                            $propertyName = trim(str_replace($class, '', $propertyName));
                            $propertyPrefix = '-';
                            break;
                        case (bool)preg_match('/^\*/', trim($propertyName)):
                            $propertyName = trim(str_replace('*', '', $propertyName));
                            $propertyPrefix = '#';
                            break;
                        default:
                            $propertyPrefix = '+';
                        // no action
                    }
                    $content[] = $this->handleExpressionItem(
                        $propertyValue,
                        $propertyName,
                        $propertyPrefix,
                        ': '
                    );
                }
                break;
            case 'array':
                $type = 'array';
                $len = count($exp);
                foreach ($exp as $elKey => $elValue) {
                    $elKey = sprintf(is_int($elKey) ? '%s' : '"%s"', $elKey);
                    $content[] = $this->handleExpressionItem($elValue, $elKey, null, ' => ');
                }
                break;
            case 'boolean':
                $content = $exp ? 'true' : 'false';
                break;
            case 'double':
            case 'integer':
                $content = (string)$exp;
                break;
            case 'string':
                $content = sprintf('"%s"', htmlspecialchars($exp, ENT_COMPAT, 'UTF-8'));
                break;
            case 'NULL':
                $content = 'NULL';
                break;
            case 'resource':
            case 'resource (closed)':
            default:
                $content = print_r($exp, 1);
        }
        $result = [
            'id'        => $id,
            'type'      => $type,
            'name'      => $name,
            'len'       => $len,
            'prefix'    => $prefix,
            'delimiter' => $delimiter,
            'content'   => $content,
        ];

        return $result;
    }
    /**
     * @param string $propertyName
     * @param mixed  $exp
     *
     * @return null|string
     */
    private function containsClassName(string $propertyName, $exp): ?string
    {
        $classes = $this->getParentClasses(new ReflectionClass($exp));

        foreach ($classes as $className => $classReflector) {
            if (strpos($propertyName, $className)) {
                return $className;
            }
        }

        return null;
    }

    /**
     * @param ReflectionClass $reflector
     *
     * @return array
     */
    private function getParentClasses(ReflectionClass $reflector): array
    {
        $result[$reflector->getName()] = $reflector->getName();

        if ($parent = $reflector->getParentClass()) {
            $result = array_merge($result, $this->getParentClasses($parent));
        }

        return $result;
    }
}