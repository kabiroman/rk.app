<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Dumper\ObjectTracker;

use SplObjectStorage as ObjectStorage;

/**
 * Class ObjectTracker
 *
 * @package Rk\App\Dumper\ObjectTracker
 */
class ObjectTracker implements ObjectTrackerInterface
{
    /**
     * @var ObjectTracker
     */
    private static $instance;

    /**
     * @return ObjectTracker
     */
    public static function getInstance(): ObjectTracker
    {
        if ( ! self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @var ObjectStorage
     */
    private $objectStorage;

    /**
     * ObjectTracker constructor.
     */
    protected function __construct()
    {
        $this->objectStorage = new ObjectStorage();
    }

    /**
     * @param object $object
     *
     * @return bool
     */
    public function isTracked($object): bool
    {
        return $this->objectStorage->contains($object);
    }

    /**
     * @param $object
     */
    public function persist($object)
    {
        $this->objectStorage->attach($object);
    }

    /**
     * @return void
     */
    public function clear()
    {
        $this->objectStorage->removeAllExcept(new ObjectStorage());
    }

    /**
     * Blocking cloning.
     */
    protected function __clone()
    {
    }

    /**
     * @throws \Exception
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize singleton");
    }
}