<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Dumper\ObjectTracker;


/**
 * Interface ObjectTrackerInterface
 *
 * @package Rk\App\Dumper\ObjectTracker
 */
interface ObjectTrackerInterface
{
    /**
     * @param object $object
     *
     * @return bool
     */
    public function isTracked($object): bool;

    /**
     * @param object $object
     *
     * @return void
     */
    public function persist($object);
}