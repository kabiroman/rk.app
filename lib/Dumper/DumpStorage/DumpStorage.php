<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Dumper\DumpStorage;

use Rk\App\Dumper\Dump;

/**
 * Class DumpStorage
 *
 * @package Rk\App\Dumper\DumpStorage
 */
class DumpStorage implements DumpStorageInterface
{
    /**
     * @var
     */
    private static $instance;

    /**
     * @var array
     */
    private $storage = [];

    /**
     * @return DumpStorage
     */
    public static function getInstance(): DumpStorage
    {
        if ( ! self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * DumpStorage constructor.
     */
    protected function __construct()
    {
    }

    /**
     * Blocking cloning.
     */
    protected function __clone()
    {
    }

    /**
     * @throws \Exception
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize singleton");
    }

    /**
     * @param Dump $dump
     */
    public function add(Dump $dump)
    {
        $this->storage[] = $dump;
    }

    /**
     * @return iterable
     */
    public function getDumps(): iterable
    {
        return $this->storage;
    }

    /**
     * @return void
     */
    public function clear()
    {
        $this->storage = [];
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return count($this->storage) === 0;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->storage);
    }
}