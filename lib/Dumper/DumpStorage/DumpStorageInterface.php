<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Dumper\DumpStorage;

use Rk\App\Dumper\Dump;

/**
 * Interface DumpStorageInterface
 *
 * @package Rk\App\Dumper
 */
interface DumpStorageInterface
{
    /**
     * @param Dump $dump
     *
     * @return void
     */
    public function add(Dump $dump);

    /**
     * @return iterable
     */
    public function getDumps(): iterable;

    /**
     * @return void
     */
    public function clear();

    /**
     * @return bool
     */
    public function isEmpty(): bool;

    /**
     * @return int
     */
    public function count(): int;
}