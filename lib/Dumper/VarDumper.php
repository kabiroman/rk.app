<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Dumper;

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\HttpResponse;

/**
 * Class VarDumper
 *
 * @package Rk\App\Dumper
 */
class VarDumper
{
    /**
     * @param $exp
     * @param $backtrace
     */
    public static function dump($exp, $backtrace)
    {
        if (php_sapi_name() == "cli") {
//            CliDumper::getInstance()
//                ->add(new Dump($exp, $backtrace));
            var_dump($exp);
        } else {
            HtmlDumper::getInstance()
                ->add(new Dump($exp, $backtrace));
        }
    }

    /**
     * Include Definitions
     */
    public static function includeDefinitions()
    {
        if (Application::getInstance()->getContext()->getResponse() instanceof HttpResponse) {
            if (Option::get("main", 'update_devsrv') == 'Y') {
                require_once __DIR__.'/../../defines.php';
            }
        }
    }
}