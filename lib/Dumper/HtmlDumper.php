<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Dumper;

use Bitrix\Main\Config\Option;
use Rk\App\Config\DevConfig;
use Rk\App\Dumper\DumpStorage\DumpStorage;
use Rk\App\Dumper\DumpStorage\DumpStorageInterface;
use Rk\App\Dumper\ObjectTracker\ObjectTracker;
use Rk\App\Dumper\VarHandler\VarHandler;
use Rk\App\Dumper\VarHandler\VarHandlerInterface;

/**
 * Class HtmlDumper
 *
 * @package Rk\App\Dumper
 */
final class HtmlDumper implements DataDumperInterface
{
    /**
     * @var DumpStorageInterface
     */
    private $dumpStorage;

    /**
     * @var VarHandlerInterface
     */
    private $varHandler;

    /**
     * @return HtmlDumper
     */
    public static function getInstance(): self
    {
        return new self(
            DumpStorage::getInstance(),
            new VarHandler(
                ObjectTracker::getInstance()
            )
        );
    }

    /**
     * Print dumps
     */
    public static function render()
    {
        if (php_sapi_name() !== 'cli') {
            $dumpStorage = static::getInstance()->dumpStorage;
            if ( ! ($dumpStorage)->isEmpty() && $dumps = $dumpStorage->getDumps()) {
                if (Option::get("main", 'update_devsrv') == 'Y') {
                    if (DevConfig::getInstance()->showDump()) {
                        require_once __DIR__.'/../../templates/var-dumper.php';
                    }
                }
            }
        }
    }

    /**
     * HtmlDumper constructor.
     *
     * @param DumpStorageInterface $dumpStorage
     * @param VarHandlerInterface  $varHandler
     */
    public function __construct(DumpStorageInterface $dumpStorage, VarHandlerInterface $varHandler)
    {
        $this->dumpStorage = $dumpStorage;
        $this->varHandler = $varHandler;
    }

    /**
     * @param Dump $dump
     *
     * @internal param $exp
     * @return   void
     */
    public function add(Dump $dump)
    {
        $dump->setResult(
            $this->varHandler->handleExpression(
                $dump->getExpression()
            )
        );

        $this->dumpStorage->add($dump);
    }
}