<?php
/**
 * This file is part of the "rk.app" RK Application Library Bitrix Module package.
 *
 *   (c) Ruslan Kabirov <kabiroman@mail.ru>
 *
 *   For the full copyright and license information, please view the LICENSE
 *   file that was distributed with this source code.
 */

namespace Rk\App\Dumper;


/**
 * Interface DataDumperInterface
 *
 * @package Rk\App\Dumper
 */
interface DataDumperInterface
{
    /**
     * @param Dump $dump
     *
     * @return void
     */
    public function add(Dump $dump);
}