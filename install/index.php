<?php

use Bitrix\Main\Loader;

class rk_app extends CModule
{
    public $MODULE_ID = "rk.app";
    public $MODULE_VERSION = "1.0.4";
    public $MODULE_VERSION_DATE = "2021-09-23 23:52:17";
    public $MODULE_NAME = "RK Application Library";
    public $MODULE_DESCRIPTION = "Library module for creating applications, customized services, solutions and automation.";
    public $MODULE_GROUP_RIGHTS = "Y";
    public $PARTNER_NAME = "Ruslan Kabirov";

    function DoInstall()
    {
        $includeModules = [
            'iblock' => 'iblock'
        ];
        foreach ($includeModules as $moduleName => $moduleTitle) {
            if ( ! Loader::includeModule($moduleName)) {
                $this->throwException(sprintf('The required module "%s" is not installed!', $moduleTitle));

                return false;
            }
        }
        self::InstallEvents();
        RegisterModule($this->MODULE_ID);

        return true;
    }

    function InstallDB($params = [])
    {

        return true;
    }

    function InstallFiles()
    {

        return true;
    }

    function DoUninstall()
    {
        self::UnInstallEvents();
        UnRegisterModule($this->MODULE_ID);

        return true;
    }

    function UnInstallDB($params = [])
    {

        return true;
    }

    function UnInstallFiles()
    {

        return true;
    }

    function InstallEvents()
    {
        if (\Bitrix\Main\Config\Option::get("main", 'update_devsrv') == 'Y') {
            RegisterModuleDependences(
                'main',
                'OnPageStart',
                'rk.app',
                \Rk\App\Dumper\VarDumper::class,
                'includeDefinitions'
            );
            RegisterModuleDependences(
                'main',
                'OnPageStart',
                'rk.app',
                \Rk\App\Toolbar\MetricsCollector::class,
                'init'
            );
            RegisterModuleDependences(
                'main',
                'OnProlog',
                'rk.app',
                \Rk\App\Toolbar\MetricsCollector::class,
                'onProlog'
            );
            RegisterModuleDependences(
                'main',
                'OnEpilog',
                'rk.app',
                \Rk\App\Dumper\HtmlDumper::class,
                'render'
            );
            RegisterModuleDependences(
                'main',
                'OnEpilog',
                'rk.app',
                \Rk\App\Toolbar\ToolbarView::class,
                'render'
            );
        }

        return true;
    }

    function UnInstallEvents()
    {
        UnRegisterModuleDependences(
            'main',
            'OnPageStart',
            'rk.app',
            \Rk\App\Dumper\VarDumper::class,
            'includeDefinitions'
        );
        UnRegisterModuleDependences(
            'main',
            'OnPageStart',
            'rk.app',
            \Rk\App\Toolbar\MetricsCollector::class,
            'init'
        );
        UnRegisterModuleDependences(
            'main',
            'OnProlog',
            'rk.app',
            \Rk\App\Toolbar\MetricsCollector::class,
            'onProlog'
        );
        UnRegisterModuleDependences(
            'main',
            'OnEpilog',
            'rk.app',
            \Rk\App\Dumper\HtmlDumper::class,
            'render'
        );
        UnRegisterModuleDependences(
            'main',
            'OnEpilog',
            'rk.app',
            \Rk\App\Toolbar\ToolbarView::class,
            'render'
        );

        return true;
    }
    /**
     * @param string $message
     */
    private function throwException(string $message)
    {
        $title = 'Module installation aborted!';

        /** @var CMain $APPLICATION */
        global $APPLICATION;
        $APPLICATION->ThrowException($message);
        $APPLICATION->IncludeAdminFile($title, __DIR__.'/error.php');
    }
}