<?php
if ( ! check_bitrix_sessid()) return;

/** @var CMain $APPLICATION */
if($ex = $APPLICATION->GetException())
{
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    CAdminMessage::ShowMessage(Array(
        "TYPE" => "ERROR",
        "MESSAGE" => GetMessage("MOD_INST_ERR"),
        "DETAILS" => $ex->GetString(),
        "HTML" => true,
    ));
    ?>
    <form action="<?echo $APPLICATION->GetCurPage()?>">
        <input type="hidden" name="lang" value="<?echo LANG?>">
        <input type="submit" name="" value="<?echo GetMessage("MOD_BACK")?>">
    </form>
    <?
}