<?php
require __DIR__.'/lib/var-dumper.php';
require __DIR__.'/lib/toolbar.php';
/** @var \Rk\App\Toolbar\ToolbarData $toolbar */
$dumps = $toolbar->getDumps();
?>
<script>
    toolbar_<?=DEV_ID_F97785862C01BB161?>(<?=json_encode((array)$toolbar)?>);
    <?/** @var \Rk\App\Dumper\Dump $dump */?>
    <?foreach ($dumps as $dump):?>
        var_dumper_<?=DEV_ID_F97785862C01BB161?>(
            'toolbar-var-dumper',
            <?=json_encode($dump->getResult())?>,
            '<?=$dump->getFile()?>',
            '<?=$dump->getLine()?>'
        );
    <?endforeach;?>
</script>