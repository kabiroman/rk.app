<?php
require_once __DIR__.'/lib/var-dumper.php';
/** @var array $dumps */
?>
<div class="var-dump-wrapper" id="page-var-dumper"></div>
<!--suppress CssUnusedSymbol -->
<style>
    .var-dump-wrapper {
        position: absolute;
        z-index: 999999;
        left: 0;
        right: 0;
        top: 200px;
        line-height: normal;
    }
    .var-dump-wrapper .dumper-frame {
        background: #000000 !important;
    }
</style>
<script>
    <?/** @var \Rk\App\Dumper\Dump $dump */?>
    <?foreach ($dumps as $dump):?>
        var_dumper_<?=DEV_ID_F97785862C01BB161?>(
            'page-var-dumper',
            <?=json_encode($dump->getResult())?>,
            '<?=$dump->getFile()?>',
            '<?=$dump->getLine()?>'
        );
    <?endforeach;?>
</script>

