<?php
?>
<!--suppress JSUnresolvedVariable -->
<script>
if (typeof toolbar_<?=DEV_ID_F97785862C01BB161?> === "undefined") {
    function toolbar_<?=DEV_ID_F97785862C01BB161?>(toolbarData) {
        let self = toolbar_<?=DEV_ID_F97785862C01BB161?>;
        if (typeof self._initialized !== "undefined") return;
        function createElement(tag, className = null, content = null, id = null) {
            let el = document.createElement(tag);
            if (className) el.className = className;
            if (content) el.innerHTML = content;
            if (id) el.id = id;
            return el;
        }
        function createToolbarItem(text, className, subMenu = null) {
            let el = createElement('div', 'toolbar-item');
            el.classList.add(className);
            let icon = createElement('i', 'toolbar-item-icon');
            let content = createElement('span', 'toolbar-item-content');
            content.innerHTML = text;
            el.appendChild(icon);
            el.appendChild(content);
            if (subMenu) {
                el.appendChild(subMenu);
            }
            return el;
        }
        function createSubMenu(items, tools = []) {
            let el = createElement('div', 'toolbar-item-sub-menu');
            let ul = createElement('ul', 'toolbar-item-sub-menu-ul');
            if (items.length <= 0) {
                return null;
            }
            items.forEach(function (item) {
                let li = createElement('li', 'sub-menu-item');
                let wrapper = createElement('div', 'sub-menu-item-wrapper');
                let name = createElement('span', 'sub-menu-item-name', item.name+': ');
                let value = createElement('span', 'sub-menu-item-value', item.value);
                wrapper.appendChild(name);
                wrapper.appendChild(value);
                li.appendChild(wrapper);
                ul.appendChild(li);
            });
            el.appendChild(ul);
            tools.forEach(function (tool) {
                el.appendChild(tool);
            });
            return el;
        }
        function getStatusClass(status) {
            switch (true) {
                case status.match(/^2/) !== null:
                    return 'toolbar-status-green';
                case status.match(/^3/) !== null:
                    return 'toolbar-status-blue';
                case status.match(/^4/) !== null:
                    return 'toolbar-status-orange';
                case status.match(/^5/) !== null:
                default:
                    return 'toolbar-status-red';
            }
        }
        function createLogList(logs = []) {
            if (logs.length <= 0) {
                return null;
            }
            let el = createElement('div', 'toolbar-log-list');
            logs.forEach(function (logListItemContent) {
                let logListItem = createElement('div', 'toolbar-log-list-item');
                logListItem.appendChild(createElement('div', 'toolbar-log-list-item-level', logListItemContent[0]));
                logListItem.appendChild(createElement('div', 'toolbar-log-list-item-mess', logListItemContent[1]));
                logListItem.appendChild(createElement('div', 'toolbar-log-list-item-in', 'in'));
                logListItem.appendChild(createElement('div', 'toolbar-log-list-item-file', logListItemContent[2]));
                logListItem.appendChild(createElement('div', 'toolbar-log-list-item-line', 'on line '+logListItemContent[3]));
                el.appendChild(logListItem);
            });
            return el;
        }
        function init(data) {
            let wrapper = createElement('div', 'mini-toolbar-wrapper');
            wrapper.classList.add('toolbar-show');

            let itemLine = createElement('div', 'toolbar-item-line', null, 'toolbar-item-line');
            wrapper.appendChild(itemLine);

            if (typeof data.httpCode !=="undefined" && data.httpCode !== null) {
                let status = createToolbarItem(data.httpCode, 'toolbar-status');
                status.classList.add(getStatusClass(data.httpCode + ''));
                itemLine.appendChild(status);
            }
            if (typeof data.site !=="undefined" && data.site !== null) {
                let site = createToolbarItem(data.site, 'toolbar-site', createSubMenu(data.siteItems));
                itemLine.appendChild(site);
            }
            if (typeof data.totalTime !=="undefined" && data.totalTime !== null) {
                let totalTime = createToolbarItem(data.totalTime, data.criticalTime ? 'toolbar-total-time-critical' : 'toolbar-total-time', createSubMenu(data.timeItems));
                itemLine.appendChild(totalTime);
            }
            if (typeof data.totalMemory !=="undefined" && data.totalMemory !== null) {
                let totalMemory = createToolbarItem(data.totalMemory, data.criticalMemory ? 'toolbar-total-memory-critical' : 'toolbar-total-memory', createSubMenu(data.memoryItems));
                itemLine.appendChild(totalMemory);
            }
            if (typeof data.logs !=="undefined" && data.logs !== null && data.logs.length > 0) {
                let logList = createLogList(data.logs);
                let logs = createToolbarItem(
                    data.logs.length,
                    'toolbar-errors',
                    createSubMenu(data.logItems,[logList])
                );
                itemLine.appendChild(logs);
            }
            if (typeof data.login !=="undefined" && data.login !== null) {
                let login = createToolbarItem(data.login, 'toolbar-login', createSubMenu(data.userItems));
                itemLine.appendChild(login);
            }
            if (typeof data.queryCount !=="undefined" && data.queryCount !== null) {
                let db = createToolbarItem(data.queryCount+' in '+data.queryTime, 'toolbar-db', createSubMenu(data.queryItems));
                itemLine.appendChild(db);
            }
            if (typeof data.debug !=="undefined" && data.dumps) {
                let debugPanel = createElement('div', 'debug-panel', null, 'toolbar-var-dumper');
                let debug = createToolbarItem(data.dumps, 'toolbar-debug', debugPanel);
                itemLine.appendChild(debug);
            }

            let btn = createElement('div', 'toolbar-toggle-btn');
            btn.classList.add('toolbar-btn-close');
            btn.onclick = function () {
                let itemLine = document.getElementById('toolbar-item-line');
                itemLine.hidden = !itemLine.hidden;
                if (itemLine.hidden) {
                    wrapper.classList.remove('toolbar-show');
                    this.classList.remove('toolbar-btn-close');
                    this.classList.add('toolbar-btn-open');
                } else {
                    wrapper.classList.add('toolbar-show');
                    this.classList.remove('toolbar-btn-open');
                    this.classList.add('toolbar-btn-close');
                }
            };
            wrapper.appendChild(btn);

            let body = document.getElementsByTagName('body')[0];
            body.appendChild(wrapper);
        }

        init(toolbarData);
        self._initialized = true;
    }
}
</script>
<!--suppress CssUnusedSymbol, CssUnknownTarget -->
<style>
    .mini-toolbar-wrapper *, .mini-toolbar-wrapper *:before, .mini-toolbar-wrapper *:after {
        box-sizing: unset;
    }
    .mini-toolbar-wrapper {
        position: fixed;
        z-index: 999998;
        right: 0;
        bottom: 0;
        min-height: 40px;
        background: #383838;
        font-family: sans-serif;
        font-size: 16px;
        line-height: normal;
    }
    .mini-toolbar-wrapper.toolbar-show {
        left: 0;
    }
    .mini-toolbar-wrapper .toolbar-toggle-btn {
        width: 40px;
        height: 40px;
        font-family: cursive;
        text-align: center;
        color: white;
        right: 0;
        top: 0;
        bottom: 0;
        position: absolute;
        cursor: pointer;
    }
    .mini-toolbar-wrapper .toolbar-toggle-btn.toolbar-btn-close {
        background: #555555;
    }
    .mini-toolbar-wrapper .toolbar-toggle-btn.toolbar-btn-close::after {
        content: url(/local/modules/rk.app/templates/svg/close.svg);
    }
    .mini-toolbar-wrapper .toolbar-toggle-btn.toolbar-btn-open {
        background: darkslategray;
    }
    .mini-toolbar-wrapper .toolbar-toggle-btn.toolbar-btn-open::after {
        content: url(/local/modules/rk.app/templates/svg/logo.svg);
    }
    .mini-toolbar-wrapper .toolbar-debug {
        padding: 0;
        margin: 0;
        width: 40px;
        height: 40px;
    }
    .mini-toolbar-wrapper .toolbar-debug .toolbar-item-icon::after {
        margin-right: 5px;
        content: url(/local/modules/rk.app/templates/svg/debug.svg);
    }
    .mini-toolbar-wrapper .toolbar-errors .toolbar-item-icon::after {
        margin-right: 7px;
        content: url(/local/modules/rk.app/templates/svg/log.svg);
    }
    .mini-toolbar-wrapper .toolbar-login .toolbar-item-icon::after {
        margin-right: 5px;
        content: url(/local/modules/rk.app/templates/svg/user.svg);
    }
    .mini-toolbar-wrapper .toolbar-db .toolbar-item-icon::after {
        margin-right: 5px;
        content: url(/local/modules/rk.app/templates/svg/query.svg);
    }
    .mini-toolbar-wrapper .toolbar-site .toolbar-item-icon::after {
        margin-right: 5px;
        content: url(/local/modules/rk.app/templates/svg/site.svg);
    }
    .mini-toolbar-wrapper .toolbar-item {
        color: white;
        padding: 10px;
        height: 20px;
        vertical-align: top;
        position: relative;
        display: inline-block;
        text-align: center;
        border-right: solid 1px #505050;
        width: max-content;
    }
    .toolbar-item-content {
        position: relative;
        vertical-align: top;
    }
    .toolbar-status {
        /*max-width: 40px;*/
    }
    .toolbar-status-green {
        background: #11703f;
    }
    .toolbar-status-blue {
        background: blue;
    }
    .toolbar-status-orange {
        background: #a46a1f;
    }
    .toolbar-status-red {
        background: red;
    }
    .toolbar-total-time-critical, .toolbar-total-memory-critical {
        background: #a46a1f !important;
    }
    .toolbar-errors {
        background: #a46a1f;
    }
    .toolbar-svg {
        padding: 0 5px 0 0 !important;
        border: none !important;
    }
    .toolbar-item-sub-menu {
        position: absolute;
        background: #505050;
        padding: 20px;
        margin: 0 0 38px;
        left: 0;
        bottom: 0;
        display: none;
        width: max-content;
        max-width: 800px;
    }
    .toolbar-item-sub-menu-ul {
        margin: 0 0 10px;
        list-style: none;
        width: max-content;
        padding: 0;
    }
    .toolbar-item-sub-menu-ul li {
        margin: 0;
    }
    .toolbar-item-sub-menu-ul li::before {
        background: none;
    }
    .toolbar-item:hover {
        background: #505050 !important;
    }
    .toolbar-item:hover .toolbar-item-sub-menu {
        display: block;
    }
    .mini-toolbar-wrapper .toolbar-debug:hover .debug-panel {
        display: block;
    }
    .toolbar-item-sub-menu .sub-menu-item .sub-menu-item-name {
        float: left;
        margin-right: 20px;
        color: #A0A0A0;
        font-weight: bold;
    }
    .toolbar-item-sub-menu .sub-menu-item .sub-menu-item-value {
        float: right;
    }
    .toolbar-item-sub-menu .sub-menu-item .sub-menu-item-wrapper {
        display: inline-block;
        width: 100%;
    }
    .toolbar-item-sub-menu .toolbar-log-list {
        position: relative;
        max-height: 300px;
        max-width: 98%;
        background: #222222;
        overflow: auto;
        text-align: left;
        padding: 5px;
        font-family: monospace;
        font-size: 14px;
        line-height: normal;
        width: max-content;
        border: solid #aaaaaa 1px;
        border-radius: 4px;
    }
    .toolbar-item-sub-menu .toolbar-log-list .toolbar-log-list-item {
        border-bottom: solid 1px #444;
        padding: 3px;
    }
    .toolbar-log-list-item .toolbar-log-list-item-level {
        display: inline-block;
        padding-right: 15px;
    }
    .toolbar-log-list-item .toolbar-log-list-item-mess {
        display: inline-block;
        padding-right: 15px;
        color: greenyellow;
    }
    .toolbar-log-list-item .toolbar-log-list-item-in {
        display: inline-block;
        padding-right: 15px;
    }
    .toolbar-log-list-item .toolbar-log-list-item-file {
        display: inline-block;
        padding-right: 15px;
        color: deepskyblue;
    }
    .toolbar-log-list-item .toolbar-log-list-item-line {
        display: inline-block;
    }
    .debug-panel {
        position: fixed;
        top: 70px;
        left: 0;
        right: 0;
        bottom: 40px;
        background: #505050;
        display: none;
        overflow: auto;
        padding: 20px 10px 10px;
    }
</style>
