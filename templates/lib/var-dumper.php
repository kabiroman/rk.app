<?php
?>
<script>
if (typeof var_dumper_<?=DEV_ID_F97785862C01BB161?> === "undefined") {
    function var_dumper_<?=DEV_ID_F97785862C01BB161?> (targetElementId, dump, file, line) {
        let targetFrame = document.getElementById(targetElementId);
        console.log(targetFrame);
        if (!targetFrame) return;

        function createElement(tag, className = null, content = null, id = null) {
            let el = document.createElement(tag);
            if (className) el.className = className;
            if (content) el.innerHTML = content;
            if (id) el.id = id;
            return el;
        }
        function createDumperFrame(dump) {
            function createItem(data) {
                function createItemName(content) {
                    let className;
                    content = '' + content;
                    switch (true) {
                        case content.match(/^[0-9]*$/) !== null:
                            className = 'dump-item-name-int';
                            break;
                        case content.match(/^"/) !== null:
                            className = 'dump-item-name-str';
                            break;
                        default:
                            className = 'dump-item-name-obj-prop';
                    }
                    return createElement('span', className, content);
                }
                function createItemType(content) {
                    let className;
                    switch (true) {
                        case (content === 'array'):
                            className = 'dump-item-type-arr';
                            break;
                        default:
                            className = 'dump-item-type-obj';
                    }
                    return createElement('span', className, content);
                }
                function createItemContent(content) {
                    let className;
                    content = '' + content;
                    switch (true) {
                        case content.match(/^[0-9]*$/) !== null:
                            className = 'dump-item-content-int';
                            break;
                        case content.match(/^[0-9]*\.[0-9]*$/) !== null:
                            className = 'dump-item-content-dbl';
                            break;
                        case content.match(/^"/) !== null:
                            className = 'dump-item-content-str';
                            break;
                        case content === 'NULL':
                            className = 'dump-item-content-null';
                            break;
                        default:
                            className = 'dump-item-content';
                    }
                    return createElement('span', className, content);
                }

                let item = createElement('li');
                if (data.prefix !== null) {
                    item.appendChild(createElement('span', 'dump-item-prefix', data.prefix));
                }
                if (data.name !== null) {
                    item.appendChild(createItemName(data.name));
                    if (data.delimiter !== null) {
                        item.appendChild(createElement('span', 'dump-item-delimiter', data.delimiter));
                    }
                }
                if (data.type !== null) {
                    if (!(data.type === 'array' && data.len === 0)) {
                        item.appendChild(createItemType(data.type));
                    }
                    if (data.len !== null && data.len > 0 && data.type === 'array') {
                        item.appendChild(createItemType(':' + data.len + ' '));
                    }
                    if (data.type !== 'array') {
                        item.appendChild(createElement('span', 'dump-item-brace', ' {'));
                        item.appendChild(createElement('span', 'dump-item-obj-id', '#' + data.id + ' '));
                    } else {
                        item.appendChild(createElement('span', 'dump-item-brace', '['));
                    }
                    if (data.len > 0) {
                        let btn = createElement('span', 'dump-item-btn', '');
                        btn.classList.add('hide');
                        item.appendChild(btn);
                    }
                }
                if (data.content !== null) {
                    if (!Array.isArray(data.content)) {
                        item.appendChild(createItemContent(data.content));
                    } else {
                        let list = document.createElement('ul');
                        list.hidden = true;
                        list.classList.add('hide');
                        data.content.forEach(function (currentValue) {
                            list.appendChild(
                                createItem(currentValue)
                            );
                        });
                        item.appendChild(list);
                    }
                }
                if (data.type !== null) {
                    if (data.type !== 'array') {
                        item.appendChild(createElement('span', 'dump-item-brace', '}'));
                    } else {
                        item.appendChild(createElement('span', 'dump-item-brace', ']'));
                    }
                }
                if (data.len > 0) {
                    let spanList = item.childNodes;
                    spanList.forEach(function (currentValue) {
                        currentValue.classList.add('cursor-pointer');
                    });
                }
                return item;
            }

            let frame = createElement('div', 'dumper-frame');
            let backtrace = createElement('div', 'dump-backtrace');
            let fileSpan = createElement('span', 'dump-file', file);
            let fileText = createElement('span', null, ' on line ');
            let lineSpan = createElement('span', 'dump-line', line);
            backtrace.appendChild(fileSpan);
            backtrace.appendChild(fileText);
            backtrace.appendChild(lineSpan);

            let list = createElement('ul', 'dump-tree');
            list.appendChild(createItem(dump));

            list.onclick = function (event) {
                if (event.target.tagName !== 'SPAN') return;
                let childContainer = event.target.parentNode.querySelector('ul');
                if (!childContainer) return;
                childContainer.hidden = !childContainer.hidden;

                let dumpItemBtn = event.target.parentNode.querySelector('span.dump-item-btn');

                if (childContainer.hidden) {
                    dumpItemBtn.classList.add('hide');
                    dumpItemBtn.classList.remove('show');
                } else {
                    dumpItemBtn.classList.add('show');
                    dumpItemBtn.classList.remove('hide');
                }
            };

            frame.appendChild(backtrace);
            frame.appendChild(list);
            list.querySelector('SPAN').click();

            return frame;
        }

        targetFrame.appendChild(createDumperFrame(dump, file, line));
    }
}
</script>
<!--suppress CssUnusedSymbol -->
<style>
    .dumper-frame {
        font-family: monospace;
        position: relative;
        /*width: 100%;*/
        background: #151515;
        color: #FF8400;;
        font-size: 13px;
        padding: 8px;
        margin-bottom: 20px;
        line-height: normal;
        text-align: left;
        border: solid 1px #AAA;
        border-radius: 4px;
    }
    .dump-backtrace {
        margin-bottom: 2px;
    }
    .dumper-frame .dump-file {
        color: #B729D9;
    }
    .dumper-frame .dump-line {
        color: #B729D9;
    }
    .dumper-frame ul {
        margin: 0;
    }
    .dumper-frame ul.dump-tree {
        padding-left: 0;
    }
    .dumper-frame li {
        margin: 0;
        list-style-type: none;
    }
    .dumper-frame li::before {
        background: none;
    }
    .dumper-frame span.cursor-pointer {
        cursor: pointer;
    }
    .dumper-frame .dump-item-btn.hide::after {
        content: '▶';
    }
    .dumper-frame .dump-item-btn.show::after {
        content: '▼';
    }
    .dump-item-obj-id {
        color: #A0A0A0;
    }
    .dump-item-name-obj-prop {
        color: #FFFFFF;
    }
    .dump-item-type-arr, .dump-item-type-obj, .dump-item-content-int, .dump-item-name-int, .dump-item-name-dbl, .dump-item-content-dbl {
        color: #1299DA;
    }
    .dump-item-name-str, .dump-item-content-str {
        color: greenyellow;
    }
    .dump-frame-footer {
        min-height: 1px;
    }
</style>
