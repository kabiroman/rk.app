<?php
define(
    'SERVER_NAME_F97785862C01BB161',
    \Bitrix\Main\Application::getInstance()->getContext()->getServer()->getServerName()
);
define('DEV_ID_F97785862C01BB161', md5(SERVER_NAME_F97785862C01BB161));

if ( ! function_exists('dump')) {
    if (\Rk\App\Config\DevConfig::getInstance()->dumperFunction()) {
        function dump($exp, ...$moreExp)
        {
            $backtrace = debug_backtrace();
            \Rk\App\Dumper\VarDumper::dump($exp, $backtrace);

            foreach ($moreExp as $exp) {
                \Rk\App\Dumper\VarDumper::dump($exp, $backtrace);
            }
        }
    }
}
if (\Rk\App\Config\DevConfig::getInstance()->exceptionHandler()) {
    if ( ! function_exists('exception_handler_F97785862C01BB161')) {
        function exception_handler_F97785862C01BB161(Throwable $e)
        {
            if (php_sapi_name() !== 'cli') {
                ob_end_clean();
                print sprintf(
                    "<pre style='font-size: 16px'>Uncaught exception (%s): in %s on line %s\n%s\n%s</pre>",
                    get_class($e),
                    $e->getFile(),
                    $e->getLine(),
                    $e->getMessage(),
                    $e->getTraceAsString()
                );

                if ( ! $e->getCode()) {
                    http_response_code(500);
                } else {
                    http_response_code($e->getCode());
                }
                \Rk\App\Toolbar\ToolbarView::render();
            } else {
                echo $e->getMessage().PHP_EOL;
                echo $e->getTraceAsString().PHP_EOL;
            }
        }
    }
    set_exception_handler('exception_handler_F97785862C01BB161');
}
if (\Rk\App\Config\DevConfig::getInstance()->errorHandler()) {
    set_error_handler(
        function ($errno, $errstr, $errfile, $errline) {
            $errors = [
                E_ERROR             => 'E_ERROR',
                E_WARNING           => 'E_WARNING',
                E_PARSE             => 'E_PARSE',
                E_NOTICE            => 'E_NOTICE',
                E_CORE_ERROR        => 'E_CORE_ERROR',
                E_CORE_WARNING      => 'E_CORE_WARNING',
                E_CORE_WARNING      => 'E_COMPILE_ERROR',
                E_COMPILE_WARNING   => 'E_COMPILE_WARNING',
                E_USER_ERROR        => 'E_USER_ERROR',
                E_USER_WARNING      => 'E_USER_WARNING',
                E_USER_NOTICE       => 'E_USER_NOTICE',
                E_STRICT            => 'E_STRICT',
                E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR',
                E_DEPRECATED        => 'E_DEPRECATED',
                E_USER_DEPRECATED   => 'E_USER_DEPRECATED',
                E_ALL               => 'E_ALL',
            ];
            \Rk\App\Toolbar\Log::getInstance()->add($errors[$errno], $errstr, $errfile, $errline);
        }
    );
}
